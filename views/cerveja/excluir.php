<?php  

	require_once("../../config.php");

	$id = $_GET['id'];
	
	$connection = new PDO("pgsql:host=127.0.0.1;dbname=brewer-php", "postgres", "postgres");
	$statement = $connection->prepare("DELETE FROM cervejas WHERE id = :id");
	$statement->bindParam(":id", $id);
	$statement->execute();
		
	$address = 'http://' . $_SERVER['SERVER_NAME'] . '/brewer-php/views/cerveja/index-cerveja.php';
	header('Location: '. $address);
	exit;
	
?>