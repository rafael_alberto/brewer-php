<?php  

	require_once("../../config.php");
	$connection = new PDO("pgsql:host=127.0.0.1;dbname=brewer-php", "postgres", "postgres");
	$statement = $connection->prepare("SELECT * FROM cervejas");
	$statement->execute();

	$results = $statement->fetchAll(PDO::FETCH_ASSOC);

	$cervejas = array();

	foreach ($results as $result) {
		$cerveja = new Cerveja();
		$cerveja->setId($result['id']);
		$cerveja->setSku($result['sku']);
		$cerveja->setNome($result['nome']);
		$cerveja->setTeorAlcoolico($result['teor_alcoolico']);
		$cerveja->setValor($result['valor']);
		$cerveja->setEstoque($result['estoque']);
		array_push($cervejas, $cerveja);
	}

?>