<?php  

	require_once("../../config.php");

	$sku = isset($_POST['sku']) ? $_POST['sku'] : NULL;
	$nome = isset($_POST['nome']) ? $_POST['nome'] : NULL;
	$teorAlcoolico = isset($_POST['teorAlcoolico']) ? $_POST['teorAlcoolico'] : NULL;
	$valor = isset($_POST['valor']) ? $_POST['valor'] : NULL;
	$estoque = isset($_POST['estoque']) ? $_POST['estoque'] : NULL;
	
	$cerveja = new Cerveja();
	$cerveja->setSku(strtoupper($sku));
	$cerveja->setNome($nome);
	$cerveja->setTeorAlcoolico(floatval(str_replace(",", ".", $teorAlcoolico)));
	$cerveja->setValor(floatval(str_replace(",", ".", $valor)));
	$cerveja->setEstoque($estoque);

	$connection = new PDO("pgsql:host=127.0.0.1;dbname=brewer-php", "postgres", "postgres");

	inserir($connection, $cerveja);

	function inserir($connection, $cerveja){
		$statement = $connection->prepare("SELECT NEXTVAL('seq_cervejas') as id");
		$statement->execute();

		$id = $statement->fetch()['id'];

		$statement = $connection->prepare("INSERT INTO cervejas (id, sku, nome, teor_alcoolico, valor, estoque) VALUES (:id, :sku, :nome, :teor_alcoolico, :valor, :estoque)");

		$statement->bindValue(":id", $id);
		$statement->bindValue(":sku", $cerveja->getSku());
		$statement->bindValue(":nome", $cerveja->getNome());
		$statement->bindValue(":teor_alcoolico", $cerveja->getTeorAlcoolico());
		$statement->bindValue(":valor", $cerveja->getValor());
		$statement->bindValue(":estoque", $cerveja->getEstoque());

		$statement->execute();
		
		$address = 'http://' . $_SERVER['SERVER_NAME'] . '/brewer-php/views/cerveja/index-cerveja.php';
		header('Location: '. $address);
		exit;
	}
?>