<!DOCTYPE html>
<html lang="pt">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<title>Brewer</title>

<link rel="stylesheet" type="text/css" href="../../static/layout/stylesheets/vendors.css" />
<link rel="stylesheet" type="text/css" href="../../static/layout/stylesheets/algaworks.min.css" />
<link rel="stylesheet" type="text/css" href="../../static/layout/stylesheets/application.css" />
<link rel="stylesheet" type="text/css" href="../../static/stylesheets/brewer.css" />

<link rel="shortcut icon" href="../../static/images/favicon.png"/>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

<script src="../../static/layout/javascripts/vendors.min.js"></script>
<script src="../../static/layout/javascripts/algaworks.min.js"></script>
<script src="../../static/javascripts/vendors/jquery.validate.min.js"></script>
<script src="../../static/javascripts/vendors/messages_pt_BR.js"></script>
<script src="../../static/javascripts/vendors/jquery.maskMoney.min.js"></script>

<div class="aw-layout-page">
	<?php  require_once '../../static/layout/fragments/barra-navegacao.html' ?>
	<?php  require_once '../../static/layout/fragments/menu-lateral.html' ?>

	<section class="aw-layout-content js-content" id="conteudo">

		<div class="page-header">
			<div class="container-fluid">
				<h1>Listagem de cervejas</h1>
			</div>
		</div>

		<div class="container-fluid">	
			<div class="table-responsive  bw-tabela-simples">
					<a class="btn  btn-link  btn-xs" title="Novo" id="btnNovo"> 
						<i class="glyphicon glyphicon-pencil"></i>
					</a>
					<table class="table table-hover">
						<thead>
							<tr>
								<th class="table-cervejas-col-id">ID</th>
								<th class="table-cervejas-col-sku">SKU</th>
								<th class="table-cervejas-col-nome">Nome</th>
								<th class="table-cervejas-col-estilo">Teor Alcoólico</th>
								<th class="table-cervejas-col-origem">Valor</th>
								<th class="table-cervejas-col-valor">Estoque</th>
								<th class="table-cervejas-col-acoes"></th>
							</tr>
						</thead>
						
						<tbody>
							<?php  

								require_once("listar.php");

								foreach ($cervejas as $cerveja) {
									echo '<tr>' .
											'<td>' . $cerveja->getId() . '</td>'.
											'<td>' . $cerveja->getSku() . '</td>' .
											'<td>' . $cerveja->getNome() . '</td>'.
											'<td>' . $cerveja->getTeorAlcoolico() . '</td>'.
											'<td>' . $cerveja->getValor() . '</td>'.
											'<td>' . $cerveja->getEstoque() . '</td>'.
											'<td class="text-center">' . 
												'<a class="btn  btn-link  btn-xs" title="Editar" 
													href="' . 'http://' . $_SERVER['SERVER_NAME'] . '/brewer-php/views/cerveja/editar.php?id=' . $cerveja->getId() . '"
													>' .
													'<i class="glyphicon glyphicon-pencil"></i>' .
												'</a>' . 
												'<a class="btn  btn-link  btn-xs" title="Excluir" 
													href="' . 'http://' . $_SERVER['SERVER_NAME'] . '/brewer-php/views/cerveja/excluir.php?id=' . $cerveja->getId() . '"
													>' .
													'<i class="glyphicon glyphicon-remove"></i>' .
												'</a>' . 
											'</td>';
								}
							?>
						</tbody>
					</table>
			</div>
		</div>
		<script type="text/javascript" src="../../javascript/cerveja/index-cerveja.js"></script>
	</section>
	
	<?php  require_once '../../static/layout/fragments/layout-footer.html' ?>
</div>

</body>
</html>