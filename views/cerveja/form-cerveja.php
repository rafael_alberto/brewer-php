<!DOCTYPE html>
<html lang="pt">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<title>Brewer</title>

<link rel="stylesheet" type="text/css" href="../../static/layout/stylesheets/vendors.css" />
<link rel="stylesheet" type="text/css" href="../../static/layout/stylesheets/algaworks.min.css" />
<link rel="stylesheet" type="text/css" href="../../static/layout/stylesheets/application.css" />
<link rel="stylesheet" type="text/css" href="../../static/stylesheets/brewer.css" />

<link rel="shortcut icon" href="../../static/images/favicon.png"/>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

<script src="../../static/layout/javascripts/vendors.min.js"></script>
<script src="../../static/layout/javascripts/algaworks.min.js"></script>
<script src="../../static/javascripts/vendors/jquery.validate.min.js"></script>
<script src="../../static/javascripts/vendors/messages_pt_BR.js"></script>
<script src="../../static/javascripts/vendors/jquery.maskMoney.min.js"></script>

<div class="aw-layout-page">
	<?php  require_once '../../static/layout/fragments/barra-navegacao.html' ?>
	<?php  require_once '../../static/layout/fragments/menu-lateral.html' ?>

	<section class="aw-layout-content js-content" id="conteudo">

		<div class="page-header">
			<div class="container-fluid">
				<h1>Cadastro de cerveja</h1>
			</div>
		</div>

		<div class="container-fluid">	
			<form method="POST" id="form" class="form-vertical js-form-loading" action="salvar.php">
				<div class="row">
					<div class="col-sm-3 form-group">
						<label for="sku" class="control-label">SKU</label>
						<input type="text" id="sku" name="sku" class="form-control" autofocus="autofocus" style="text-transform: uppercase;"/>
					</div>
							
					<div class="col-sm-9 form-group">
						<label for="nome" class="control-label">Nome</label>
						<input type="text" id="nome" name="nome" class="form-control" />
					</div>					
				</div>
						
				<div class="row">
					<div class="col-sm-3 form-group">
						<label for="teorAlcoolico" class="control-label">Teor alcoólico</label>
						<div class="input-group">
							<input type="text" id="teorAlcoolico" name="teorAlcoolico" class="form-control js-decimal" />
							<div class="input-group-addon">%</div>
						</div>
					</div>

					<div class="col-sm-3 form-group">
						<label for="valor" class="control-label">Valor</label>
						<div class="input-group">	
							<div class="input-group-addon">R$</div>
							<input type="text" id="valor" name="valor" class="form-control js-decimal" />
						</div>
					</div>
							
					<div class="col-sm-3 form-group">
						<label for="estoque" class="control-label">Estoque</label>
						<input type="text" id="estoque" name="estoque" class="form-control js-plain" />
					</div>
				</div>
				
				<div class="form-group">
					<button class="btn  btn-primary" type="submit">Salvar</button>
				</div>
			</form>
		</div>
		<script type="text/javascript" src="../../javascript/cerveja/form-cerveja.js"></script>
	</section>
	
	<?php  require_once '../../static/layout/fragments/layout-footer.html' ?>
</div>

</body>
</html>