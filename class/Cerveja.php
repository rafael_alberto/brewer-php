<?php  

	class Cerveja {

		private $id;
		private $sku;
		private $nome;
		private $teorAlcoolico;
		private $valor;
		private $estoque;

		public function getId():int{
			return $this->id;
		}

		public function setId($id){
			$this->id = $id;
		}

		public function getSku():string{
			return $this->sku;
		}

		public function setSku($sku){
			$this->sku = $sku;
		}

		public function getNome():string {
			return $this->nome;
		}

		public function setNome($nome){
			$this->nome = $nome;
		}

		public function getTeorAlcoolico():float{
			return $this->teorAlcoolico;
		}

		public function setTeorAlcoolico($teorAlcoolico){
			$this->teorAlcoolico = $teorAlcoolico;
		}

		public function getValor():float{
			return $this->valor;
		}

		public function setValor($valor){
			$this->valor = $valor;
		}

		public function getEstoque():int{
			return $this->estoque;
		}

		public function setEstoque($estoque){
			$this->estoque = $estoque;
		}

	}
	
?>