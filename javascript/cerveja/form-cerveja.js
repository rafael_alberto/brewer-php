$(function() {
	
	$('#form').validate({
    	rules: {
        	sku: {
            	required: true
        	},
            nome: {
                required: true
            }
    	}, 
    	highlight: function(element) {
        	$(element).closest('.form-group').addClass('has-error');
    	},
    	unhighlight: function(element) {
        	$(element).closest('.form-group').removeClass('has-error');
    	}
	});

    var decimal = $('.js-decimal');
    decimal.maskMoney({ decimal: ',', thousands: '.' });

    var plain = $('.js-plain');
    plain.maskMoney({ precision: 0, thousands: '.' });
});